# k8s

**master.sh, node.sh and common.sh**

The three shell scripts get called as provisioners during the Vagrant run to configure the cluster.

_common.sh: – A self-explanatory list of commands which installs docker, kubeadm, kubectl and kubelet on all the nodes. Also, disables swap._
_master.sh: – contains commands to initialize master, install the calico plugin, metrics server, and kubernetes dashboard. Also, copies the kube-config, join.sh, and token files to the configs directory._
_node.sh:- reads the join.sh command from the configs shared folder and join the master node. Also, copied the kubeconfig file to /home/vagrant/.kube location to execute kubectl commands._

common.sh installs kubernetes version 1.20.6-00 to have the same cluster version for CKA/CKAD and CKS preparation. If you would like the latest version, remove the version number from the command.

Original: https://github.com/scriptcamp/vagrant-kubeadm-kubernetes
